<?php

namespace Drupal\idealyzer\Controller;

use Drupal\Core\Controller\ControllerBase;

class IdealyzerController extends ControllerBase {

  public function statusPage() {
    $header = [
        ['data' => $this->t('Compliance Check'), 'class' => [RESPONSIVE_PRIORITY_MEDIUM]],
        ['data' => $this->t('Status'), 'class' => [RESPONSIVE_PRIORITY_LOW]],
    ];
    $rows = [];

    // Introductory text as previously defined
    $intro_text = '<p>This tool is part of a comprehensive compliance scanning suite intended to assist in aligning with the <a href="https://digital.gov/guides/site-scanning/" target="_blank">21st Century IDEA Act</a>. For a detailed site scan, refer to the provided link.</p>';
    $build['intro_text'] = [
        '#type' => 'markup',
        '#markup' => $intro_text,
        '#allowed_tags' => ['a', 'p'],
    ];

    // Run all checks
    $checks = $this->runAllChecks();

    // Display the results in a table format with conditional coloring
    foreach ($checks as $checkName => $result) {
        $status_class = $this->determineStatusClass($result); // Determine the class based on result
        $rows[] = [
            ['data' => $this->t($checkName), 'class' => ['idealyzer-check-name']],
            ['data' => $this->t($result), 'class' => ['idealyzer-check-result', $status_class]],
        ];
    }

    $build['idealyzer_status_table'] = [
        '#type' => 'table',
        '#header' => $header,
        '#rows' => $rows,
        '#attributes' => ['id' => 'idealyzer-status-table'],
    ];
    $build['#attached']['library'][] = 'idealyzer/status-styles';

    return $build;
}


private function determineStatusClass($result) {
  // Lowercase the result for case insensitive comparison
  $lowerResult = strtolower($result);

  // Define patterns that indicate an error or non-compliance
  if (strpos($lowerResult, 'not detected') !== false || 
      strpos($lowerResult, 'error') !== false || 
      strpos($lowerResult, 'missing') !== false ||
      strpos($lowerResult, 'not using') !== false) {
      return 'status-error'; // Use 'error' styling for negative outcomes or issues
  } else if (strpos($lowerResult, 'present but') !== false) {
      return 'status-warning'; // Use 'warning' styling for warnings or partial compliance
  } else {
      return 'status-ok'; // Use 'ok' styling for all clear or fully compliant statuses
  }
}



  private function runAllChecks() {
    $results = [];
    $results['IPv6 Compliance'] = $this->checkIPv6();
    $results['Digital Analytics Program'] = $this->checkDAP();
    $results['USWDS Styles'] = $this->checkUSWDSVersion();
    $results['USWDS JavaScript'] = $this->checkUSWDSJavaScript();
    $results['External Resources'] = $this->checkThirdPartyResources();
    $results['Agency Domains'] = $this->checkAgencyDomains();

    return $results;
}


  private function checkIPv6() {
    // Example check for IPv6 (simplified)
    $host = $_SERVER['SERVER_ADDR'];
    if (filter_var($host, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
      return "Server is using IPv6.";
    } else {
      return "Server is not using IPv6.";
    }
  }

  private function checkDAP() {
    // Check for DAP script presence in the HTML output of the homepage
    $client = \Drupal::httpClient();
    try {
        $response = $client->get(\Drupal::request()->getSchemeAndHttpHost());
        $body = (string) $response->getBody();
        if (preg_match('/src="([^"]*dap\.digitalgov\.gov\/Universal-Federated-Analytics-Min\.js[^"]*)"/i', $body, $scriptSrc)) {
            // DAP script is present, now check for agency ID
            if (preg_match('/agency=([^&"]+)/', $scriptSrc[1], $agencyMatch)) {
                return "DAP script is present with Agency ID: " . htmlspecialchars($agencyMatch[1]);
            } else {
                return "DAP script is present but Agency ID is not specified.";
            }
        } else {
            return "DAP script is missing.";
        }
    } catch (\Exception $e) {
        return "Error checking DAP: " . $e->getMessage();
    }
}

private function checkUSWDSVersion() {
  $client = \Drupal::httpClient();
  $homepageUrl = \Drupal::request()->getSchemeAndHttpHost();

  try {
      $response = $client->get($homepageUrl);
      $body = (string) $response->getBody();

      // Enhanced regex to handle various <link> tag formats
      // This regex matches both double and single quotes, any order of attributes, and optional query strings
      $pattern = '/<link[^>]*href=["\']([^"\']*uswds[^"\']*\.css(?:\?[^"\']*?)?)["\'][^>]*>/i';
      if (preg_match($pattern, $body, $cssLink)) {
          // Check if the URL is absolute or relative and correct it
          $cssUrl = $cssLink[1];
          if (strpos($cssUrl, 'http') !== 0) {  // If URL is relative
              $cssUrl = rtrim($homepageUrl, '/') . '/' . ltrim($cssUrl, '/');
          }

          // Return confirmation of USWDS CSS file detection
          return "USWDS CSS file detected at URL: " . $cssUrl;
      } else {
          return "USWDS CSS file not detected on the homepage.";
      }
  } catch (\Exception $e) {
      // Detailed error message for better debugging
      return "Error checking USWDS: " . $e->getMessage();
  }
}


private function checkUSWDSJavaScript() {
  $client = \Drupal::httpClient();
  $homepageUrl = \Drupal::request()->getSchemeAndHttpHost();

  try {
      $response = $client->get($homepageUrl);
      $body = (string) $response->getBody();

      // Regex to detect USWDS JavaScript files in the HTML
      // This regex looks for script tags with src attributes that include 'uswds' and end with '.js',
      // accounting for potential query strings
      $pattern = '/<script[^>]+src=["\']([^"\']*uswds[^"\']*\.js(?:\?[^"\']*?)?)["\'][^>]*>/i';
      if (preg_match($pattern, $body, $jsLink)) {
          // Normalize the URL
          $jsUrl = $jsLink[1];
          if (strpos($jsUrl, 'http') !== 0) {  // If URL is relative
              $jsUrl = rtrim($homepageUrl, '/') . '/' . ltrim($jsUrl, '/');
          }

          // Return confirmation of USWDS JavaScript file detection
          return "USWDS JavaScript file detected at URL: " . $jsUrl;
      } else {
          return "USWDS JavaScript file not detected on the homepage.";
      }
  } catch (\Exception $e) {
      // Detailed error message for better debugging
      return "Error checking USWDS JavaScript: " . $e->getMessage();
  }
}

private function checkThirdPartyResources() {
    $client = \Drupal::httpClient();
    $homepageUrl = \Drupal::request()->getSchemeAndHttpHost();
    
    try {
        $response = $client->get($homepageUrl);
        $body = (string) $response->getBody();
        
        // Patterns to match external resources: CSS, images, and fonts
        $patterns = [
            'CSS' => '/<link[^>]+?href="([^"]+?\.css)"[^>]*?>/i',
            'Images' => '/<img[^>]+?src="([^"]+?)"[^>]*?>/i',
            'Fonts' => '/@font-face[^{]*?{[^}]*?url\("([^"]+?)"\)[^}]*?}/i'
        ];
        
        $resources = [];
        foreach ($patterns as $type => $pattern) {
            preg_match_all($pattern, $body, $matches);
            if (!empty($matches[1])) {
                // Filter out internal resources
                $externalResources = array_filter($matches[1], function($url) use ($homepageUrl) {
                    return strpos($url, $homepageUrl) === false && preg_match('/^https?:\/\//i', $url);
                });
                if (!empty($externalResources)) {
                    $resources[$type] = array_unique($externalResources);
                }
            }
        }
        
        if (!empty($resources)) {
            $results = [];
            foreach ($resources as $type => $urls) {
                $results[] = "$type detected: " . implode(', ', $urls);
            }
            return implode('; ', $results);
        } else {
            return "No external resources detected.";
        }
    } catch (\Exception $e) {
        return "Error checking third-party resources: " . $e->getMessage();
    }
}


private function checkAgencyDomains() {
  // Get the current domain
  $currentDomain = $_SERVER['HTTP_HOST'];

  // Explode the domain into parts
  $domainParts = explode('.', $currentDomain);
  $partCount = count($domainParts);

  // Assume the last part is the TLD
  $tld = end($domainParts);

  // Verify that the TLD is '.gov'
  if ($tld !== 'gov') {
      return "Error: The domain is not a .gov domain. Current domain: $currentDomain";
  }

  // Determine if it's a primary domain or a subdomain
  if ($partCount > 2) {
      // More than two parts suggest a subdomain
      $subdomain = implode('.', array_slice($domainParts, 0, $partCount - 2));
      return "This is a subdomain: $subdomain of " . implode('.', array_slice($domainParts, -2, 2));
  } else {
      // Just two parts suggest a primary domain
      return "This is a primary .gov domain with potential subdomains: $currentDomain";
  }
}



}
