<?php

use Drupal\system\SystemManager;

/**
 * Implements hook_system_status_alter().
 */
function idealyzer_system_status_alter(array &$items, SystemManager $system_manager) {
  $items['idealyzer'] = [
    'title' => t('Idealyzer Status'),
    'value' => t('OK'), // Dynamic status value
    'description' => t('Detailed status message here...'),
    'severity' => REQUIREMENT_OK,
  ];
}
